# Prose Realm API
Did you lose track on your Light Novel reading history? 

Forget what chapter that you last read? Or the last volume you read? 

`Prose Realm - Light Novel Database` will help you in the future, so that you can always keep track on your Light Novel reading history.

Prose Realm consist of 3 Application:
- API (This Repository)
- Android Apps (User)
- Desktop Apps (Admin)

<br>

# Installation
## This API are using Laravel
### Requirement
- Minimum PHP 8
- Composer
- SQL Database (MySQL, MariaDB, PostgreSQL, SQLite, etc)

### Get Started
Clone Repositoty
```bash
git clone https://gitlab.com/prose-realm/api.git
cd MyLnList.v2
```
Install Laravel dependency
```bash
composer install
```
copy .env.example
```bash
cp .env.example .env
```
Configure your .env (don't forget to create database)

Database Migration
```bash
php artisan migrate -seed
```
====================

Make sure you already setup Web Server such as Apache 2, NGINX, etc.

If you don't want to use Web Server such as Apache2, NGINX, etc. You can use Laravel Aritsan Serve
```bash
php artisan serve
```

<br>

# API Documentation
`Disclaimer`: This is just an example, may be different when you try 

<br>

## Authorization Type: Bearer Token

<br>

# 📁 Collection: Auth 
## End-point: Login
### Method: POST
>```
>{{url}}/api/v0/auth/login
>```
### Body (**raw**)

```json
{
    "email":"admin@gmail.com",
    "password":"k3v1n#test"
}
```

### Response: 302
```json
{
    "request": "login",
    "status": 302,
    "message": "Login Success. DO NOT SHARE YOUR TOKEN!",
    "token": "1|4Gejo1MM9796BHyzea4mDwyvR3QLGefmJxnBDaA7"
}
```

### Response: 401
```json
{
    "request": "login",
    "status": 401,
    "message": "Login Failed"
}
```
⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Check Token
### Method: POST
>```
>{{url}}/api/v0/auth/check
>```
### Response: 200
```json
{
    "request": "Check Token",
    "status": 200,
    "message": "Token Found",
    "data": {
        "user_id": "da4b9237bacccdf19c0760cab7aec4a8359010b0",
        "googleID": null,
        "email": "admin@gmail.com",
        "username": "admin",
        "role": "admin",
        "name": "Kevin Luis Linuhung",
        "email_verified_at": null,
        "created_at": "2023-05-18T23:02:30.000000Z",
        "updated_at": "2023-05-18T23:02:30.000000Z",
        "deleted_at": null
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Logout
### Method: POST
>```
>{{url}}/api/v0/auth/logout
>```
### Response: 200
```json
{
    "request": "logout",
    "status": 200,
    "message": "Logout Success"
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Logout All
### Method: POST
>```
>{{url}}/api/v0/auth/logoutall
>```
### Response: 200
```json
{
    "request": "logoutAll",
    "status": 200,
    "message": "Logout Success"
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃
# 📁 Collection: Library 


## End-point: Get All Light Novel
### Method: GET
>```
>{{url}}/api/v0/lightnovel
>```
### Response: 200
```json
{
    "request": "getAllLightNovel",
    "status": 200,
    "message": "Success",
    "data": [
        {
            "id": 1,
            "title": "Gakusen Toshi Asterisk",
            "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\n\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
            "status": null,
            "cover": "{{url}}/assets/images/lightnovel/no_cover.jpg",
            "verif": 1
        },
        {
            "id": 2,
            "title": "Date a Live",
            "description": "This is date a Live 2",
            "status": null,
            "cover": "{{url}}/assets/images/lightnovel/no_cover.jpg",
            "verif": 1
        },
        {
            "id": 4,
            "title": "Strike the Blood",
            "description": "No Description",
            "status": null,
            "cover": "{{url}}/assets/images/lightnovel/no_cover.jpg",
            "verif": 1
        },
        ...
        ...
        ...
        {
            "id": 89,
            "title": "Gakusen Toshi Asterisk5",
            "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\\n\\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
            "status": null,
            "cover": "{{url}}/assets/images/lightnovel/89/cover.jpg",
            "verif": 1
        }
    ]
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Get Light Novel
### Method: GET
>```
>{{url}}/api/v0/lightnovel/1
>```
### Response: 200
```json
{
    "request": "getLightNovel",
    "status": 200,
    "message": "Success",
    "data": {
        "id": 1,
        "title": "Gakusen Toshi Asterisk",
        "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\n\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
        "status": null,
        "cover": "{{url}}/assets/images/lightnovel/no_cover.jpg",
        "verif": 1
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Add New Light Novel
### Method: POST
>```
>{{url}}/api/v0/lightnovel
>```
### Body formdata

|Param|value|Type|
|---|---|---|
|title|Gakusen Toshi Asterisk10|text|
|description|In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.Ayato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.|text|
|status||text|
|cover|/Volume_1_Cover.webp|file|
|verif|1|text|


### Response: 200
```json
{
    "request": "addLightNovel",
    "status": 200,
    "message": "Success",
    "data": {
        "title": "Gakusen Toshi Asterisk10",
        "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\\n\\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
        "status": null,
        "verif": "1",
        "cover": "{{url}}/assets/images/lightnovel/92/cover.webp"
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Update Light Novel
### Method: PUT
>```
>{{url}}/api/v0/lightnovel
>```
### Body formdata

|Param|value|Type|
|---|---|---|
|id|89|text|
|title|Gakusen Toshi Asterisk5|text|
|description|In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.Ayato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.|text|
|status||text|
|cover||file|
|verif|1|text|


### Response: 200
```json
{
    "request": "updateLightnovel",
    "status": 200,
    "message": "Success",
    "data": {
        "id": "89",
        "title": "Gakusen Toshi Asterisk5",
        "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\\n\\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
        "status": null,
        "verif": "1"
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃
# 📁 Collection: User 


## End-point: Add Light Novel
### Method: POST
>```
>{{url}}/api/v0/user/lightnovel
>```
### Body (**raw**)

```json
{
    "ln_id": "1",
    "volume": "1",
    "chapter": "0",
    "status": "PTR",
    "score": "0"
}
```

### Response: 200
```json
{
    "request": "addUserLightNovel",
    "status": 200,
    "message": "Success",
    "data": {
        "user_history_id": "f58fbaa9f64023f42cb0380a0333c3880fa6bcdf",
        "user_id": "da4b9237bacccdf19c0760cab7aec4a8359010b0",
        "ln_id": "1",
        "volume": 0,
        "chapter": 0,
        "status": "PTR",
        "score": 0,
        "updated_at": "2023-05-18T17:07:15.000000Z",
        "created_at": "2023-05-18T17:07:15.000000Z"
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Get All Light Novel
### Method: GET
>```
>{{url}}/api/v0/user/lightnovel
>```
### Response: 200
```json
{
    "request": "getAllUserLightNovel",
    "status": 200,
    "message": "Success",
    "data": [
        {
            "user_history_id": "f58fbaa9f64023f42cb0380a0333c3880fa6bcdf",
            "user_id": "da4b9237bacccdf19c0760cab7aec4a8359010b0",
            "ln_id": "1",
            "volume": "0",
            "chapter": "0",
            "status": "PTR",
            "score": "0",
            "created_at": "2023-05-18T17:07:15.000000Z",
            "updated_at": "2023-05-18T17:07:15.000000Z"
        }
    ]
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Get Light Novel
### Method: POST
>```
>{{url}}/api/v0/user/lightnovel/1
>```
### Body (**raw**)

```json
{
    "ln_id": 1
}
```

### Response: 200
```json
{
    "request": "getUserLightNovel",
    "status": 200,
    "message": "Success",
    "data": {
        "user_history_id": "f58fbaa9f64023f42cb0380a0333c3880fa6bcdf",
        "user_id": "da4b9237bacccdf19c0760cab7aec4a8359010b0",
        "ln_id": "1",
        "volume": "0",
        "chapter": "0",
        "status": "PTR",
        "score": "0",
        "created_at": "2023-05-18T17:07:15.000000Z",
        "updated_at": "2023-05-18T17:07:15.000000Z"
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Update Light Novel
### Method: PUT
>```
>{{url}}/api/v0/user/lightnovel
>```
### Body (**raw**)

```json
{
    "ln_id": "1",
    "volume": "1",
    "chapter": "0",
    "status": "PTR",
    "score": "0"
}
```

### Response: 200
```json
{
    "request": "updateUserLightNovel",
    "status": 200,
    "message": "Success",
    "data": {
        "user_history_id": "f58fbaa9f64023f42cb0380a0333c3880fa6bcdf",
        "user_id": "da4b9237bacccdf19c0760cab7aec4a8359010b0",
        "ln_id": "1",
        "volume": "1",
        "chapter": "0",
        "status": "PTR",
        "score": "0",
        "created_at": "2023-05-18T17:07:15.000000Z",
        "updated_at": "2023-05-18T17:08:20.000000Z"
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Add Request
### Method: POST
>```
>{{url}}/api/v0/request
>```
### Body (**raw**)

```json
{
    "title" : "Gakusen Toshi Asterisk 1",
    "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\n\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet."
}
```

### Response: 200
```json
{
    "request": "addRequestLightNovel",
    "status": 200,
    "message": "Success",
    "data": {
        "user_request_id": "f44a852285f8d0f3751c52686b0fc0bb51ba4d2e",
        "user_id": "da4b9237bacccdf19c0760cab7aec4a8359010b0",
        "title": "Gakusen Toshi Asterisk 1",
        "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\n\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
        "updated_at": "2023-05-18T17:08:30.000000Z",
        "created_at": "2023-05-18T17:08:30.000000Z"
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Get All Request
### Method: GET
>```
>{{url}}/api/v0/request
>```
### Response: 200
```json
{
    "request": "getAllRequestLightNovel",
    "status": 200,
    "message": "Success",
    "data": [
        {
            "user_request_id": "f44a852285f8d0f3751c52686b0fc0bb51ba4d2e",
            "user_id": "da4b9237bacccdf19c0760cab7aec4a8359010b0",
            "title": "Gakusen Toshi Asterisk 1",
            "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\n\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
            "approved": 0,
            "reason": null,
            "created_at": "2023-05-18T17:08:30.000000Z",
            "updated_at": "2023-05-18T17:08:30.000000Z"
        }
    ]
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Get Request Detail
### Method: GET
>```
>{{url}}/api/v0/request/f44a852285f8d0f3751c52686b0fc0bb51ba4d2e
>```

### Response: 200
```json
{
    "request": "getRequestLightNovel",
    "status": 200,
    "message": "Success",
    "data": {
        "user_request_id": "f44a852285f8d0f3751c52686b0fc0bb51ba4d2e",
        "user_id": "da4b9237bacccdf19c0760cab7aec4a8359010b0",
        "title": "Gakusen Toshi Asterisk 1",
        "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\n\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
        "approved": 0,
        "email": "admin@gmail.com",
        "created_at": "2023-05-18T17:08:30.000000Z"
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃

## End-point: Approve Light Novel
### Method: POST
>```
>{{url}}/api/v0/request/approve
>```
### Body (**raw**)

```json
{
    "user_request_id": "f44a852285f8d0f3751c52686b0fc0bb51ba4d2e"
}
```

### Response: 200
```json
{
    "request": "approveRequestLightNovel",
    "status": 200,
    "message": "Success",
    "data": {
        "title": "Gakusen Toshi Asterisk 1",
        "description": "In the previous century, an unprecedented disaster known as the Invertia drastically reformed the world. The powers of existing nations declined significantly, paving the way for a conglomerate called the Integrated Empire Foundation to assume control. But more importantly, the Invertia led to the emergence of a new species of humans who are born with phenomenal physical capabilities—the Genestella. Its elite are hand-picked across the globe to attend the top six schools, and they duel amongst themselves in entertainment battles called Festas.\n\nAyato Amagiri is a scholarship transfer student at the prestigious Seidoukan Academy, which has recently been suffering from declining performances. Through a series of events, he accidentally sees the popular Witch of Resplendent Flames, Julis-Alexia von Riessfeld, half-dressed! Enraged, Julis challenges him to a duel for intruding on her privacy. After said duel is voided by the student council president, Ayato reveals that he has no interest in Festas. Instead, he has enrolled in the academy to investigate the whereabouts of his missing elder sister. But when a more devious plot unravels, Ayato sets out to achieve victory, while being surrounded by some of the most talented Genestella on the planet.",
        "status": "null",
        "verif": 1,
        "id": 93
    }
}
```


⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃ ⁃
