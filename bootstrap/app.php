<?php

use Illuminate\Http\Request;
use App\Http\Middleware\TokenHasRole;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        api: __DIR__.'/../routes/api.php',
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
    )
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->alias([
            'tokenRole' => TokenHasRole::class
        ]);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        $exceptions->shouldRenderJsonWhen(function (Request $request, Throwable $e) {
            if($request->is('api/*')){
                if(get_class($e) == "Symfony\Component\HttpKernel\Exception\NotFoundHttpException"){
                    return response()->json([
                        'error' => [
                            'status' => 404,
                            'message' => 'Endpoint not found'
                        ]
                    ], 404);
                }
                else{
                    return response()->json([
                        'error' => [
                            'status' => 500,
                            'message' => $e->getMessage(),
                        ]
                    ]);
                }
            }
        });
    })->create();
