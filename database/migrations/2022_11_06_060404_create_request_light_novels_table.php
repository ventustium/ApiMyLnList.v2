<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_light_novel.requests', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->default(DB::raw('uuid_generate_v4()'));
            $table->foreignId('user_id')->nullable()->index()->constrained('gate.users');
            $table->string('title');
            $table->longText('description');
            $table->integer('approved')->default(0);
            $table->string('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_light_novel.request');
    }
};
