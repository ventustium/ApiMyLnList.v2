<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gate.roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('gate.user_roles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('role_id')->nullable()->index()->constrained('gate.roles');
            $table->foreignId('user_id')->nullable()->index()->constrained('gate.users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gate.role');
    }
};
