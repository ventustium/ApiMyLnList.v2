<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Gate\Role;
use App\Models\Gate\UserRole;
use App\Models\Gate\GoogleAuth;
use Illuminate\Database\Seeder;
use App\Models\LightNovel\Series;
use Illuminate\Support\Facades\DB;
use App\Models\LightNovel\User\UserLightNovel;
use App\Models\LightNovel\User\UserLightNovelHistory;

class UserToProseRealm extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $users = DB::connection("MyLnList")->table("users")->get();

        $role = Role::where('name', 'USER')->first();

        foreach ($users as $u) {
            $user = User::updateOrCreate([
                'email' => $u->email,
            ], [
                'username' => $u->username,
            ]);

            $userRole = UserRole::updateOrCreate([
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]);

            $googleAuth = GoogleAuth::updateOrCreate([
                'user_id' => $user->id,
                'email' => $u->email,
                'google_id' => $u->googleID,
            ]);

            $light_novel_history = DB::connection("MyLnList")->table("users_history")->where('googleID', $googleAuth->google_id)->get();
            foreach ($light_novel_history as $history) {
                //get series_id
                $series = Series::where('id', $history->idLN)->first();
                UserLightNovel::updateOrCreate([
                    'user_id' => $user->id,
                    'series_id' => $series->id,
                ], [
                    'volume' => $history->volume,
                    'chapter' => $history->chapter,
                    'status' => $history->status,
                    'score' => $history->score,
                    'created_at' => $history->time_Stamp,
                    'updated_at' => $history->time_Stamp
                ]);
            }

            $light_novel_history_log = DB::connection('MyLnList')->table('users_history_log')->where('googleID', $googleAuth->google_id)->get();
            foreach ($light_novel_history_log as $history) {
                $userLightNovel = UserLightNovel::where('series_id', $history->idLN)->where('user_id', $user->id)->first();
                UserLightNovelHistory::updateOrCreate([
                    'series_id' => $userLightNovel->id,
                    'volume' => $history->volume,
                    'chapter' => $history->chapter,
                    'status' => $history->status,
                    'score' => $history->score,
                    'created_at' => $history->time_Stamp,
                    'updated_at' => $history->time_Stamp
                ]);
            }
        }

        $admins = DB::connection("MyLnList")->table("admins")->get();

        $role = Role::where('name', 'ADMIN')->first();
        foreach ($admins as $admin) {
            $user = User::updateOrCreate([
                'username' => $admin->username,
            ], [
                'password' => $admin->password
            ]);
            $userRole = UserRole::updateOrCreate([
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]);
        }
    }
}
