<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\LightNovel\Request\RequestLightNovel;

class UserRequestToProseRealm extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $user_request = DB::connection('MyLnList')->table('users_request')->get();

        foreach ($user_request as $ur) {
            $user_old = DB::connection('MyLnList')->table('users')->where('id', $ur->userID)->first();

            if ($user_old) {
                $user = User::with('google')->whereRelation('google', 'google_id', $user_old->googleID)->first();
                RequestLightNovel::updateOrCreate([
                    'user_id' => $user->id,
                    'title' => $ur->title,
                    'description' => $ur->description,
                ], [
                    'approved' => $ur->verif,
                ]);
            }
        }
    }
}
