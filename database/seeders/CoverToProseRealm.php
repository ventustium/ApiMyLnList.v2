<?php

namespace Database\Seeders;

use App\Models\LightNovel\Series;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Models\MyLnList\LnList2;
use App\Models\LightNovel\Volume;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class CoverToProseRealm extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $lnList = DB::connection('MyLnList2')->table('lnList')->get();

        foreach ($lnList as $ln) {
            if (!$ln->cover) {
                continue;
            }
            DB::beginTransaction();
            // Get image from old database
            $volume = Volume::updateOrCreate(
                [
                    'series_id' => $ln->id,
                    'volume_number' => '-1',
                ],
                [
                    'is_main' => true
                ]
            );
            $imageType = null;
            if ($ln->cover) {
                $imageType = explode('.', $ln->cover)[1]; // e.g., "1.webp" -> "webp"
            }
            $s3_path = 'lightnovel/series/volume/cover/' . $volume->uuid . '.' . $imageType;
            if (!Storage::disk('s3')->exists($s3_path ?? '-1')) {
                $response = Http::get(env('MyLnListAPI') . "/assets/images/lightnovel/" . $ln->cover);
                if (!$response->successful()) {
                    continue;
                }

                $image = Storage::disk('s3')->put($s3_path, $response);
                if (!$image) {
                    continue;
                }
            }
            // Now that we have the $volume instance, we can update the cover_image_path
            $volume->cover_image_path = $s3_path;
            $volume->save();
            DB::commit();
        }
    }
}
