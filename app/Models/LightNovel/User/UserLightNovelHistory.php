<?php

namespace App\Models\LightNovel\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserLightNovelHistory extends Model
{
    use HasFactory;
    protected $table = "user_light_novel.histories";

    protected $guarded= [];

    public function series(){
        return $this->belongsTo(UserLightNovel::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
