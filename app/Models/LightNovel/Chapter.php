<?php

namespace App\Models\LightNovel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    use HasFactory;
    protected $table = "light_novel.chapters";
    protected $guarded = [];

    public function volume()
    {
        return $this->belongsTo(Volume::class);
    }
}
