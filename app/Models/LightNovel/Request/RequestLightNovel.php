<?php

namespace App\Models\LightNovel\Request;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RequestLightNovel extends Model
{
    use HasFactory;
    protected $table = "user_light_novel.requests";

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
