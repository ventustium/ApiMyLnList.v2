<?php

namespace App\Models;

use App\Models\Gate\Role;
use App\Models\Gate\UserRole;
use App\Models\Gate\GoogleAuth;
use App\Models\Gate\User as GateUser;
use App\Models\LightNovel\User\UserLightNovel;
use App\Models\LightNovel\Request\RequestLightNovel;

class User extends GateUser
{
    public function UserLightNovel()
    {
        return $this->hasMany(UserLightNovel::class);
    }

    public function RequestLightNovel()
    {
        return $this->hasMany(RequestLightNovel::class);
    }

    public function google()
    {
        return $this->hasOne(GoogleAuth::class);
    }

    public function roles()
    {
        return $this->hasMany(UserRole::class);
    }

    public function hasRole($role) : bool
    {
        $ids = Role::whereIn('name', $role)->pluck('id');
        if ($ids->isEmpty()) return false;

        return $this->roles()->whereIn('role_id', $ids)->exists();
    }
}
