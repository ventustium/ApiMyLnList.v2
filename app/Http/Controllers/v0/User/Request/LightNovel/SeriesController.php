<?php

namespace App\Http\Controllers\v0\User\Request\LightNovel;

use App\Models\LightNovel\Series as LightNovel;
use App\ResponseController;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\LightNovel\Request\RequestLightNovel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SeriesController extends Controller
{
    use ResponseController;

    //Add Light Novel Request
    public function addRequestLightNovel(Request $request){
        $validation = Validator::make($request->all(),[
            'title' => 'required | unique:series',
        ], [
            'title.unique' => 'Light Novel already exist on database'
        ]);

        if($validation->fails()){
            return $this->validationError(__FUNCTION__, $validation->errors());
        }

        $validation = Validator::make($request->all(),[
            'title' => 'required | unique:requests',
            'description' => 'required'
        ], [
            'title.unique' => 'Light Novel already requested'
        ]);

        if($validation->fails()){
            return $this->validationError(__FUNCTION__, $validation->errors());
        }


        $data = RequestLightNovel::create([
            'user_id' => auth('sanctum')->user()->id,
            'title' => $request->title,
            'description' => $request->description
        ]);

        return $this->success(__FUNCTION__, $data);
    }

    //Get All Light Novel Request List
    public function getAllRequestLightNovel(){
        $user = auth('sanctum')->user();
        if($user->hasRole(['ADMIN'])) {
            $data = RequestLightNovel::get();
            return $this->success(__FUNCTION__, $data);
        }
        $data = RequestLightNovel::where('user_id', $user->id)->get();
        return $this->success(__FUNCTION__, $data);
    }

    //Get Detail Light Novel Request List
    public function getRequestLightNovel($uuid){
        $RequestLightNovel = RequestLightNovel::where('uuid', $uuid)->with('user')->first();
        if (!isset($RequestLightNovel)){
            return $this->notFound(__FUNCTION__);
        }

        $data = [
            'uuid' => $RequestLightNovel->uuid,
            'user_id' => $RequestLightNovel->user_id,
            'title' => $RequestLightNovel->title,
            'description' => $RequestLightNovel->description,
            'approved' => $RequestLightNovel->approved,
            'email' => $RequestLightNovel->user->email,
            'created_at' => $RequestLightNovel->created_at
        ];
        return $this->success(__FUNCTION__, $data);
    }

    //Approve Light Novel Request
    public function approveRequestLightNovel(Request $request){
        $validation = Validator::make($request->all(),[
            'uuid' => 'required'
        ]);

        if($validation->fails()){
            return $this->validationError(__FUNCTION__, $validation->errors());
        }

        $RequestLightNovel = RequestLightNovel::where('uuid', $request->uuid)->first();

        $validation = Validator::make($RequestLightNovel->toArray(),[
            'title' => 'unique:series',
        ], [
            'title.unique' => 'Light Novel already exist on database'
        ]);

        if($validation->fails()){
            return $this->validationError(__FUNCTION__, $validation->errors());
        }

        $RequestLightNovel->approved = 1;
        $RequestLightNovel->save();

        $data = LightNovel::create([
            'title' => $RequestLightNovel->title,
            'description' => $RequestLightNovel->description,
            'status' => 'null',
            'verif' => 1
        ]);
        return $this->success(__FUNCTION__, $data);
    }
}
