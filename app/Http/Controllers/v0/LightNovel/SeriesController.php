<?php

namespace App\Http\Controllers\v0\LightNovel;

use App\Models\LightNovel\Series as LightNovel;
use App\ResponseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SeriesController extends Controller
{
    use ResponseController;

    public function index()
    {
        $data = LightNovel::with(['volume' => function ($query) {
            $query->where('is_main', true)->select('id', 'series_id', 'cover_image_path');
        }])->select('id', 'uuid', 'title',)->get()->map(function ($series) {
            $cover = $this->getCoverPath($series->volume->first()->cover_image_path ?? null);
            return [
                'id' => $series->uuid,
                'title' => $series->title,
                'cover' => $cover
            ];
        });

        return $this->success(__FUNCTION__, $data);

        // return response(json_encode($response, JSON_PRETTY_PRINT), 200, [
        //     'content-type' => "application/json"
        // ]);
    }

    public function show($uuid)
    {
        $data = LightNovel::with(['volume'])->where('uuid', $uuid)->first();
            $cover = $this->getCoverPath($data->volume->where('is_main', true)->first()->cover_image_path ?? null);
            $data->cover = $cover;
            foreach ($data->volume as $volume) {
                $cover = $this->getCoverPath($volume->cover_image_path ?? null);
                $volume->cover = $cover;
                unset($volume->cover_image_path);
            }
        
        if (!$data)
            return $this->notFound(__FUNCTION__);

        $result = json_decode(json_encode($data), true);
        $result['id'] = $result['uuid'];
        unset($result['id']);

        return $this->success(__FUNCTION__, $data);
    }

    public function addLightNovel(Request $request)
    {

        $data = $request->all();

        $validation = Validator::make($request->all(), [
            'title' => 'required | unique:series',
            'description' => 'required',
        ]);

        if ($validation->fails()) {
            return $this->validationError(__FUNCTION__, $validation->errors());
        }

        $LightNovel = LightNovel::create([
            'title' => $request->title,
            'description' => $request->description,
            'status' => $request->status ?? null,
            'verif' => $request->verif ?? null,
        ]);

        return $this->success(__FUNCTION__, $data);
    }

    public function updateLightnovel(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'uuid' => 'required',
        ]);

        if ($validation->fails()) {
            return $this->validationError(__FUNCTION__, $validation->errors());
        }

        $LightNovel = LightNovel::where('uuid', $request->uuid)->first();
        if (!$LightNovel) {
            return $this->notFound(__FUNCTION__);
        }

        $data = $request->all();

        if (isset($request->input['title'])) {
            if ($request->title != $LightNovel->title) {
                $validation = Validator::make($data, [
                    'title' => 'required | unique:series'
                ]);

                if ($validation->fails()) {
                    return $this->validationError(__FUNCTION__, $validation->errors());
                }
            }
        }

        $LightNovel->update($data);

        return $this->success(__FUNCTION__, $data);
    }

    private function getCoverPath($value)
    {
        // Check if the cover image path is set
        if (isset($value) && $value) {
	        return Storage::disk('s3')->url($value);
        } else {
            return Storage::disk('s3')->url('/lightnovel/no_cover.jpg');
        }
    }
}
