<?php

namespace App;

trait ResponseController
{
    public function notFilledCorrect($request){
        $response = [
            'request' => $request,
            'status' => 400,
            'message' => "Please check your input"

        ];
        // return response()->pJson($response, 400);
        return response()->json($response, 400);
    }

    public function validationError($request, $data){
        $response = [
            'request' => $request,
            'status' => 400,
            'message' => "Please check your input",
            'error' => $data

        ];
        return response()->json($response, 400);
    }

    public function unauthorized($request){
        $response = [
            'request' => $request,
            'status' => 401,
            'message' => "Unauthorized",
        ];
        return response()->json($response, 401);
    }

    public function invalidToken($request){
        $response = [
            'request' => $request,
            'status' => 401,
            'message' => "Invalid Token",
        ];
        return response()->json($response, 401);
    }

    public function success($request, $data){
        $response = [
            'request' => $request,
            'status' => 200,
            'message' => "Success",
            'data' => $data
        ];
        return response()->json($response, 200);
    }

    public function found($request, $data){
        $response = [
            'request' => $request,
            'status' => 200,
            'message' => "Found",
            'data' => $data
        ];
        return response()->json($response, 200);
    }

    public function notFound($request){
        $response = [
            'request' => $request,
            'status' => 404,
            'message' => "Not found",
        ];
        return response()->json($response, 404);
    }
}
