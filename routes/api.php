<?php

use Illuminate\Http\Request;
use App\Http\Middleware\TokenHasRole;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\v0\AuthController;
use App\Http\Middleware\EnsureTokenIsValid;
use App\Http\Controllers\v0\LightNovel\SeriesController;
use App\Http\Controllers\v0\User\Request\LightNovel\SeriesController as RequestLightNovelController;
use App\Http\Controllers\v0\User\LightNovel\SeriesController as UserSeries;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:sanctum');

Route::name('api.')->group(function () {
    Route::prefix('v0')->name('v0.')->group(function () {
        Route::get('/docs', function () {
            return view('api-documentation');
        });

        Route::get('/resources/{path?}', function ($path = null) {
                // return $path;
                if (!$path) {
                return response()->json(['error' => 'File not found.'], 404);
            }
            if(Storage::disk('s3')->exists($path)) {
                $data = Storage::disk('s3')->mimeType($path);
            return response()->make(Storage::disk('s3')->get($path), 200, ['Content-Type' => $data]);
            }
            return response()->json(['error' => 'File not found'], 404);
        })->where('path', '.*');

        Route::prefix('auth')->name('auth.')->group(function () {
            Route::post('/login', [AuthController::class, 'login'])->name('login');
            Route::middleware([EnsureTokenIsValid::class])->group(function () {
                Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
                Route::post('/logoutall', [AuthController::class, 'logoutAll'])->name('logoutAll');
                Route::post('/check', [AuthController::class, 'checkToken'])->name('checkToken');
            });
        });

        // LightNovel Routes
        Route::prefix('lightnovel')->name('lightnovel.')->group(function () {
            Route::prefix('series')->name('series.')->group(function () {
                Route::get('/', [SeriesController::class, 'index'])->name('index');
                Route::get('/{uuid}', [SeriesController::class, 'show'])->name('show');
                //Middleware check if ADMIN
                Route::middleware(['tokenRole:ADMIN'])->group(function () {
                    Route::post('/', [SeriesController::class, 'addLightNovel'])->name('store');
                    Route::put('/', [SeriesController::class, 'updateLightNovel'])->name('update');
                });
            });
        });

        //Middleware check if authenticated
        Route::middleware([EnsureTokenIsValid::class])->group(function () {
            // Request Routes
            Route::prefix('request')->name('request.')->group(function () {
                Route::prefix('lightnovel')->name('lightnovel.')->group(function () {
                    Route::prefix('series')->name('series.')->group(function () {
                        Route::get('/', [RequestLightNovelController::class, 'getAllRequestLightNovel'])->name('index');
                        Route::get('/{uuid}', [RequestLightNovelController::class, 'getRequestLightNovel'])->name('show');
                        Route::post('/request', [RequestLightNovelController::class, 'addRequestLightNovel'])->name('store');
                        Route::post('/approve', [RequestLightNovelController::class, 'approveRequestLightNovel'])->name('approve')->middleware(['tokenRole:ADMIN']);
                    });
                });
            });

            Route::name('user.')->prefix('user')->group(function () {
                Route::name('lightnovel.')->prefix('lightnovel')->group(function () {
                    Route::name('series.')->prefix('series')->group(function () {
                        Route::get('', [UserSeries::class, 'getAllUserLightNovel'])->name('index');
                        Route::get('/{uuid}', [UserSeries::class, 'getUserLightNovel'])->name('show');
                        Route::post('/', [UserSeries::class, 'addUserLightNovel'])->name('store');
                        Route::put('/', [UserSeries::class, 'updateUserLightNovel'])->name('update');
                    });
                });
            });
        });
    });
});
